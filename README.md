# multi-plot-tlx-likert

Plot TLX- and Likert-Scales with multiple conditions.
That is you have condition 1 and condition 2 and for each one you have tested various other conditions
(see example plots, red = condition 1, blue = condition 2).

![TLX example](examples/tlx_example.svg)
![Likert example](examples/likert_example.svg)

# Usage
    *-plot.py input questionsfile outdir

Input file format (CSV):

    header1 conditions to be desiplayed on side
    header2 case 1 or case 2
    each line answers for question (median for Likert, mean for TLX)

Question file format (Text):

    each line a single question / statement


With ``swap_col.py`` you can swap columns and rows of your CSV file.
