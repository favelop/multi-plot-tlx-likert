#!/bin/python

# tlx-plot.py
# Copyright (C) 2022 favelop.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# ussage: 
# tlx-plot.py input questionsfile outdir

# inputfile format:
# header1 conditions to be desiplayed on side
# header2 case 1 or case 2
# each line answers (median) for question

# questionsfile: each line a question / statement

import svgwrite
import sys

SCALE = 2

C1 = "#c90000"
C2 = "#0000c9"

SVG_WIDTH = 150
SVG_HEIGHT = 100
HEIGHT = 10
COL_SPACING = 4.375
ROW_SPACING = 10
ACTIVE_HEIGHT_ADJUST = -0.5
ACTIVE_HEIGHT = 11
FONT_SIZE = 4
TOP_OFFSET = FONT_SIZE * 2
LEFT_OFFSET = 15
SCALA_LEFT_OFFSET = 80

NUM_QUESTIONS = 20

SVG_WIDTH *= SCALE
SVG_HEIGHT *= SCALE
HEIGHT *= SCALE
COL_SPACING *= SCALE
ROW_SPACING *= SCALE
ACTIVE_HEIGHT_ADJUST *= SCALE
ACTIVE_HEIGHT *= SCALE
FONT_SIZE *= SCALE
TOP_OFFSET *= SCALE
LEFT_OFFSET *= SCALE
SCALA_LEFT_OFFSET * SCALE

def createSvg(filepath):
    svgd = svgwrite.Drawing(filename = filepath, size = (SVG_WIDTH, SVG_HEIGHT))
    # from inskcape
    #svgd.defs.add('<marker style="overflow:visible" id="TriangleOutM" refX="0.0" refY="0.0" orient="auto"> <path  transform="scale(0.4)"  style="fill-rule:evenodd;stroke:#c90000;stroke-width:1pt;stroke-opacity:1;fill:#c90000;fill-opacity:1" d="M 5.77,0.0 L -2.88,5.0 L -2.88,-5.0 L 5.77,0.0 z " id="path1006" /></marker>')

    markerTop = svgd.marker(style="overflow:visible", id="TriangleOutM", refX="0.0", refY="0.0", orient="auto")

    # red point as marker
    markerTop.add(svgd.path(
        transform="scale(" + str(0.4 * SCALE) + ")",
        style="fill-rule:evenodd;stroke:"+C1+";stroke-width:1pt;stroke-opacity:1;fill:"+C1+";fill-opacity:1",
        d="M 5.77,0.0 L -2.88,5.0 L -2.88,-5.0 L 5.77,0.0 z "))

    # add marker to defs section of the drawing
    svgd.defs.add(markerTop)
    
    # SAME BUT SCALE IS NEGATIVE AND COLOR IS DIFFERENT!
    markerBottom = svgd.marker(style="overflow:visible", id="TriangleInM", refX="0.0", refY="0.0", orient="auto")
    markerBottom.add(svgd.path(
        transform="scale(" + str(-0.4 * SCALE) + ")",
        style="fill-rule:evenodd;stroke:"+C2+";stroke-width:1pt;stroke-opacity:1;fill:"+C2+";fill-opacity:1",
        d="M 5.77,0.0 L -2.88,5.0 L -2.88,-5.0 L 5.77,0.0 z "))
    svgd.defs.add(markerBottom)

    return svgd

def addTitle(svgd, x, y, text):
    
    svgd.add(svgd.text(text,
        insert=("50%", y + HEIGHT / 2 + (FONT_SIZE - 1) / 2),
        stroke='none',
        text_anchor="middle",
        font_family="Charter",
        fill=svgwrite.rgb(15, 15, 15, '%'),
        font_size=FONT_SIZE)
    )
    

def addText(svgd, x, y, text):
    svgd.add(svgd.text(text,
        insert=(x, y + HEIGHT / 2 + (FONT_SIZE - 1) / 2),
        stroke='none',
        font_family="Roboto",
        fill=svgwrite.rgb(15, 15, 15, '%'),
        font_size=FONT_SIZE)
    )

def addScala(svgd, x, y, value, value2):
    # draw markers
    for i in range(0, NUM_QUESTIONS):
        svgd.add(svgd.line(
            start = (x + i * COL_SPACING, y + 2),
            end = (x + i * COL_SPACING, y + HEIGHT - 2),
            style = "stroke:#000000;stroke-opacity:0.5;stroke-width:0.1;"))
        
    # actually value 10.5 is middle but we count from zero
    svgd.add(svgd.line(
            start = (x + 9.5 * COL_SPACING, y),
            end = (x + 9.5 * COL_SPACING, y + HEIGHT),
            style = "stroke:#000000;stroke-opacity:0.5;stroke-width:0.5;"))
        
    #dashed strike through
    svgd.add(svgd.line(
            start = (x , y + HEIGHT / 2),
            end = (x + (NUM_QUESTIONS - 1) * COL_SPACING, y + HEIGHT / 2),
            style = "stroke:#000000;stroke-opacity:0.5;stroke-width:0.1;stroke-dasharray: 1,1;"))
    
    # current value
    if value >= 0:
        svgd.add(svgd.line(
            start = (x + (value - 1) * COL_SPACING, y + ACTIVE_HEIGHT_ADJUST),
            end = (x + (value - 1) * COL_SPACING, y + HEIGHT / 2),
            style = "stroke:"+C1+";stroke-opacity:1;stroke-width:0.5;marker-start:url(#TriangleOutM);fill:"+C1+";fill-opacity:1"))
        # value on right side
        svgd.add(svgd.text(str(round(value,2)), insert=(x + NUM_QUESTIONS * COL_SPACING, y + ACTIVE_HEIGHT_ADJUST + FONT_SIZE), stroke='none', font_family="Roboto", fill=C1, font_size=FONT_SIZE))
        
    # second value
    if value2 >= 0:
        svgd.add(svgd.line(
            start = (x + (value2 - 1) * COL_SPACING, y + HEIGHT / 2),
            end = (x + (value2 - 1) * COL_SPACING, y + ACTIVE_HEIGHT),
            style = "stroke:"+C2+";stroke-opacity:1;stroke-width:0.5;marker-end:url(#TriangleInM);fill:"+C2+";fill-opacity:1"))
        # value on right side
        svgd.add(svgd.text(str(round(value2,2)), insert=(x + NUM_QUESTIONS * COL_SPACING, y + HEIGHT / 2 + FONT_SIZE), stroke='none', font_family="Roboto", fill=C2, font_size=FONT_SIZE))

if len(sys.argv) < 3:
    print("Not enough arguments: inputcsv questions outdir")
    sys.exit()

infile = open(sys.argv[1],'r')
lines = infile.readlines()
infile.close()

questionsfile = open(sys.argv[2],'r')
qlines = questionsfile.readlines()
questionsfile.close()

outdir = sys.argv[3]

#load to memory double array

headerDelay = []
headerRun = []

num = 0
for line in lines:
    line2 = line.rstrip('\n')
    values = line2.split(",")
    
    if num == 0:
        headerDelay = values
    elif num == 1:
        headerRun = values
    elif len(values) > 1: # that is no empty line
        #rows.append(values)
        # now print svg for each row that is for each questions
        svgd = createSvg(outdir + "/" + str(num-1) + ".svg")
        i = 0 # cause we modify i
        o = 0 # offset counter
        
        addTitle(svgd, 0, 0, qlines[num - 2])
        
        while i < len(headerDelay):
            # match both together
            delay = headerDelay[i]
            topPos = TOP_OFFSET + ROW_SPACING / 2 + o * (HEIGHT + ROW_SPACING / 2)
            addText(svgd, LEFT_OFFSET, topPos, delay + "ms")
            if i + 1 < len(headerDelay) and headerDelay[i] == headerDelay[i+1] and headerRun[i] == "1" and headerRun[i+1] == "2": # just to be sure xD
                addScala(svgd, SCALA_LEFT_OFFSET, topPos, float(values[i]), float(values[i+1]))
                i += 2 # and skip value
            else:
                addScala(svgd, SCALA_LEFT_OFFSET, topPos, float(values[i]), -1)
                i += 1
            o += 1 # so we do not have gaps
        
        svgd.save()
    num += 1
    
    #for i in range (2,16):
    #    run = values[1]
    #    delay = values[0]
    #    topPos = i * (HEIGHT + ROW_SPACING / 2)
    #    addText(5, topPos, run + ": " + delay + "ms")
    #    addScala(25, topPos, float(values[i]) - 1)
    #break;
