#!/bin/python

# swap_col.py input


import sys

infile = open(sys.argv[1],'r')
lines = infile.readlines()
infile.close()

columns = []

for line in lines:
    line2 = line.rstrip('\n')
    values = line2.split(",")
    columns.append(values)

# first row determines length
for i in range(0,len(columns[0])):
    newline = ""
    for k in range(0,len(columns)):
        newline = newline + "," + columns[k][i]
    print(newline[1:]) #skip first line
